//main
function handleDocSo() {
  console.log("yes");
  var numberValue = document.getElementById("txt-number").value * 1;
  document.getElementById("result").innerHTML =`<h2>${docSo(numberValue)}</h2>` 
}

function docSo(i) {
  var letter = ["không","một","hai","ba","bốn","năm","sáu","bảy","tám","chín",];
  var content = "";
  var hangTram, hangChuc, hangDonVi;
  var hangTram = parseInt(i / 100);
  var hangChuc = parseInt((i % 100) / 10);
  var hangDonVi = i % 10;

  if (hangTram == 0 && hangChuc == 0 && hangDonVi == 0) {
    return "";
  }
  //handle Hàng trăm

  if (hangTram != 0) {
    content += letter[hangTram] + " trăm ";
  }
  //handle Hàng Chục
  switch (hangChuc) {
    case 0:
      {
        content += " linh ";
      }
      break;
    case 1:
      {
        content += " mười ";
      }
      break;
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      content += letter[hangChuc] + " mươi ";
  }
  //handle Hàng Đơn Vị
  switch (hangDonVi) {
    case 1:
      if (hangChuc != 0 && hangChuc != 1) {
        content += " mốt ";
      } else {
        content += letter[hangDonVi];
      }
      break;
    case 5:
      if (hangChuc == 0) {
        content += letter[hangDonVi];
      } else {
        content += " lăm ";
      }
      break;
    default:
      if (hangDonVi != 0) {
        content += letter[hangDonVi];
      }
      break;
  }
  return content;
}
